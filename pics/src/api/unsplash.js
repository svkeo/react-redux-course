import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID U9UYPR0ueU0Zln6PqCsrGOx3ayb7te7CyENKiqGRg1Q',
  },
});
